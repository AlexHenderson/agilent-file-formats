# Agilent Formats #
### Alex Henderson ###

This is a collection of code to read the Agilent (formerly Varian) FTIR file formats. Currently only MATLAB/Octave code is available. Hopefully more languages will be added in due course. If you have code to read Agilent formats in another language, please let me know and I'll put a link to your code from here. 

The file formats currently handled are those for single FPA image tiles and for mosaics of FPA image tiles. Spectra are not currently handled, but can be easily saved in JCAMP-DX or Grams (spc) formats which are more generally accessible. 

Running;

	agilentFile

will determine which file format is presented and open the file directly. Optionally, running either of; 
	  
	agilentImage
	agilentMosaic

will open the file type specified.

# Agilent FTIR Image File Reader #
### Alex Henderson ###

This source code will read an Agilent FTIR hyperspectral image file set.

The Agilent (formerly Varian) image file formats consists of a number of files with extensions: .dms, .dmt, .drd, .dmd, for the mosaic version, or .bsp, .dat, .seq for the single tile version. This code will determine which format the data is in and open the file accordingly.   

The source code is licenced under the GPL v3 licence. This means if you change it, and release your new version, you **MUST** release the source code too. If you would like to use the code under different licensing conditions, please contact the author.

If you find this code fails for whatever reason, please let the author know using the issues section of the BitBucket website: [https://bitbucket.org/AlexHenderson/agilent-file-formats/issues](https://bitbucket.org/AlexHenderson/agilent-file-formats/issues)

Alex Henderson <[alex.henderson@manchester.ac.uk](mailto:alex.henderson@manchester.ac.uk "alex.henderson@manchester.ac.uk")>

## Usage ##

### MATLAB Version ###
	%   Function: agilentFile
	%   Usage: [wavenumbers, data, width, height, filename, acqdate] = agilentFile();
	%   Usage: [wavenumbers, data, width, height, filename, acqdate] = agilentFile(filename);
	%
	%   Extracts the spectra from either an Agilent single tile FPA image, or a
	%   mosaic of tiles.
	%   Plots an image of the total signal.
	%
	%   input:
	%   'filename' string containing the full path to either a .seq file (for a
	%       single tile) or a .dms file (mosaic) (optional)
	% 
	%   output:
	%   'wavenumbers' is a list of the wavenumbers related to the data
	%   'data' is a 3D cube of the data in the file (height x width x wavenumbers)
	%   'width' is the width of the image in pixels (rows)
	%   'height' is the height of the image in pixels (columns)
	%   'filename' is a string containing the full path to the opened file
	%   'acqdate' is a string containing the date and time of acquisition
	%
	%                     *******Caution******* 
	%   This code is a hack of the Agilent format and the location of the data
	%   within the file may vary. Always check the output to make sure it is
	%   sensible. If you have a file that doesn't work, please contact Alex. 
	%
	%   Copyright (c) 2017, Alex Henderson 
	%   Contact email: alex.henderson@manchester.ac.uk
	%   Licenced under the GNU General Public License (GPL) version 3
	%   http://www.gnu.org/copyleft/gpl.html
	%   Other licensing options are available, please contact Alex for details
	%   If you use this file in your work, please acknowledge the author(s) in
	%   your publications. 
	%
	%   version 1.0, June 2017

 
# Agilent FTIR Mosaic Image File Reader #
### Alex Henderson ###

This source code will read an Agilent FTIR hyperspectral mosaicked image file set.

The Agilent (formerly Varian) mosaic file format consists of a number of files with extensions: .dms, .dmt, .drd, .dmd. 

The source code is licenced under the GPL v3 licence. This means if you change it, and release your new version, you **MUST** release the source code too. If you would like to use the code under different licensing conditions, please contact the author.

If you find this code fails for whatever reason, please let the author know using the issues section of the BitBucket website: [https://bitbucket.org/AlexHenderson/agilent-file-formats/issues](https://bitbucket.org/AlexHenderson/agilent-file-formats/issues)

If you make use of this file in your work, please consider citing the appropriate version. Release 1.0 is available in the Zenodo Repository: [DOI:10.5281/zenodo.399238](http://doi.org/10.5281/zenodo.399238 "DOI:10.5281/zenodo.399238")

Alex Henderson <[alex.henderson@manchester.ac.uk](mailto:alex.henderson@manchester.ac.uk "alex.henderson@manchester.ac.uk")>

## Usage ##

### MATLAB Version ###
	% Function: agilentMosaic
	% Usage: 
	%   [wavenumbers, data, width, height, filename, acqdate] = agilentMosaic();
	%   [wavenumbers, data, width, height, filename, acqdate] = agilentMosaic(filename);
	%   [wavenumbers, data, width, height, filename, acqdate] = agilentMosaic(filename, keepme);
	%
	% Purpose:
	%   Extracts the spectra from an Agilent (formerly Varian) .dmt/.dms/.dmd
	%   file combination. Plots an image of the total signal.
	%
	%  input:
	%   'filename' string containing the full path to the .dms file (optional)
	%   'keepme' vector of wavenumber values in pairs indicating the limits of regions to retain (optional)
	% 
	%  output:
	%   'wavenumbers' is a list of the wavenumbers related to the data
	%   'data' is a 3D cube of the data in the file ((fpaSize x X) x (fpaSize x Y) x wavenumbers)
	%   'width' is width in pixels of the entire mosaic
	%   'height' is height in pixels of the entire mosaic
	%   'filename' is a string containing the full path to the .dms file
	%   'acqdate' is a string containing the date and time of acquisition
	%
	%                     *******Caution******* 
	%   This code is a hack of the Agilent format and the location of the data
	%   within the file may vary. Always check the output to make sure it is
	%   sensible. If you have a file that doesn't work, please contact Alex.
	%
	%   Copyright (c) 2011 - 2017, Alex Henderson 
	%   Contact email: alex.henderson@manchester.ac.uk
	%   Licenced under the GNU General Public License (GPL) version 3
	%   http://www.gnu.org/copyleft/gpl.html
	%   Other licensing options are available, please contact Alex for details
	%   If you use this file in your work, please acknowledge the author(s) in
	%   your publications. 
	%
	%       version 6.1, June 2017
# Agilent FTIR Single Tile Image File Reader #
### Alex Henderson ###

This source code will read an Agilent FTIR hyperspectral image file set.

The Agilent (formerly Varian) single tile file format consists of a number of files with extensions: .bsp, .dat, .seq. 

The source code is licenced under the GPL v3 licence. This means if you change it, and release your new version, you **MUST** release the source code too. If you would like to use the code under different licensing conditions, please contact the author.

If you find this code fails for whatever reason, please let the author know using the issues section of the BitBucket website: [https://bitbucket.org/AlexHenderson/agilent-file-formats/issues](https://bitbucket.org/AlexHenderson/agilent-file-formats/issues)

Alex Henderson <[alex.henderson@manchester.ac.uk](mailto:alex.henderson@manchester.ac.uk "alex.henderson@manchester.ac.uk")>

## Usage ##

### MATLAB Version ###
	%   Function: agilentImage
	%   Usage: [wavenumbers, data, width, height, filename, acqdate] = agilentImage();
	%   Usage: [wavenumbers, data, width, height, filename, acqdate] = agilentImage(filename);
	%
	%   Extracts the spectra from an Agilent (formerly Varian) single tile FPA image.
	%   Plots an image of the total signal.
	%
	%   input:
	%   'filename' string containing the full path to the .seq file (optional)
	% 
	%   output:
	%   'wavenumbers' is a list of the wavenumbers related to the data
	%   'data' is a 3D cube of the data in the file (height x width x wavenumbers)
	%   'width' is the width of the image in pixels (rows)
	%   'height' is the height of the image in pixels (columns)
	%   'filename' is a string containing the full path to the .bsp file
	%   'acqdate' is a string containing the date and time of acquisition
	%
	%                     *******Caution******* 
	%   This code is a hack of the Agilent format and the location of the data
	%   within the file may vary. Always check the output to make sure it is
	%   sensible. If you have a file that doesn't work, please contact Alex. 
	%
	%   Copyright (c) 2011 - 2017, Alex Henderson 
	%   Contact email: alex.henderson@manchester.ac.uk
	%   Licenced under the GNU General Public License (GPL) version 3
	%   http://www.gnu.org/copyleft/gpl.html
	%   Other licensing options are available, please contact Alex for details
	%   If you use this file in your work, please acknowledge the author(s) in
	%   your publications. 
	%
	%   version 5.0, June 2017
